angular.module('mweb', [])
.controller('PurchaseCtrl', function($scope, $http) {
  $scope.purchase = {
    num_seats: 0
  };
  $http.get('section.php').success(function(data) {
    $scope.sections = data;
  });
  function computeAmount() {
    $scope.totalCost = 0;
    if($scope.purchase.section) {
      $scope.totalCost = $scope.purchase.section.price * $scope.purchase.num_seats;
    }
  }
  $scope.$watch('purchase.section', computeAmount);
  $scope.$watch('purchase.num_seats', computeAmount);
  $scope.submit = function(purchase) {
    console.log('purchase', purchase);
    $http.post('/purchase.php', purchase).success(function(data) {
      console.log('data', data);
    });
  };
});