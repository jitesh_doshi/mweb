<?php
include_once 'config.php';
global $database;
$dbh = new PDO($database['url'], $database['username'], $database['password']);

header('Content-Type: application/json');
$sth = $dbh->prepare('SELECT * FROM mweb_section');
$sth->execute();
$data = $sth->fetchAll(PDO::FETCH_OBJ);
print(json_encode($data));