CREATE TABLE `mweb_purchase` (
	`id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'auto-generated primary key',
	`buyer_name` VARCHAR(100) NOT NULL COMMENT 'full name of buyer',
	`buyer_email` VARCHAR(100) NOT NULL COMMENT 'email address of buyer',
	`seating_section` VARCHAR(100) NOT NULL COMMENT 'name of seating section for the ticket',
	`num_seats` INT(11) NOT NULL COMMENT 'number of seats purchased',
	PRIMARY KEY (`id`)
)
COMMENT='ticket purchase records'
;

CREATE TABLE `mweb_section` (
	`id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'auto-generated primary key',
	`name` VARCHAR(100) NOT NULL COMMENT 'section name',
	`price` DECIMAL(10,2) NOT NULL COMMENT 'base price per seat',
	PRIMARY KEY (`id`)
)
COMMENT='seating section records'
;
