<?php

if (preg_match('/^application\/x-www-form-urlencoded/', $_SERVER['HTTP_CONTENT_TYPE'])) {
  header('Content-Type: text/plain');
  print_r($_SERVER);
  print_r($_GET);
  print_r($_POST);
  print_r($_REQUEST);
}

if (preg_match('/^application\/json/', $_SERVER['HTTP_CONTENT_TYPE'])) {
  header('Content-Type: application/json');
  $data = json_decode(file_get_contents('php://input'));
  print(json_encode($data));
}
